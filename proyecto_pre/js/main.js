//Relacionado con llamadas 'fetch' (para uso en IE11)
import '@babel/polyfill';
import 'whatwg-fetch';

//Uso de librería d3
import { select, event } from 'd3-selection';
import { nest } from 'd3-collection';
import { scaleBand, scaleLinear } from 'd3-scale';
import { max } from 'd3-array';
import { axisBottom, axisLeft } from 'd3-axis';
import { line as d3line, curveCatmullRom } from 'd3-shape';

//Variables y funciones relativas a Covid-19
import { covidDataVacunas } from './common_utilities/api-constants';
import { meses, tsvToJson } from './covid-helpers';
import { getNumberWithThousandsPoint } from './common_utilities/dom-logic-elements';
import { drawTooltip, tooltipDiv } from './common_utilities/tooltip';

//Necesario para importar los estilos de forma automática en la etiqueta 'style' del html final
import '../css/main.scss';

let dataVacunas = [];

function createCovidCharts(){
    Promise.all([
        window.fetch(covidDataVacunas).then((response) => {return response.text()})
    ]).then(function(responses){
        dataVacunas = tsvToJson(responses[0]);
        vacunasDoubleChart(dataVacunas);
		drawTooltip();
    }).catch(function(error){console.log(error);})    
}

createCovidCharts();

/////// 
/////// 
/////// Desarrollo de visualizaciones + Helpers 
///////
///////

/////// Doble gráfico de vacunación ///////
function vacunasDoubleChart(dataVacunas) {
    //Variables internas al bloque
    let selector = document.getElementById('vacunas-provincias');
    let histogram = document.getElementById('vaccine-histogram');

    let opcionDefecto = '20';
    let dataProvincias = [], filteredData = [], maxData = 0;

    let margin, width, height, svg, x, xAxis, y, yAxis, line;

    //Agrupamos por provincia (y a nivel nacional)
	let dataProvinciasAux = nest()
        .key(function(d) {return d.Cod;})
        .entries(dataVacunas);

    dataProvinciasAux = dataProvinciasAux.sort(comparativa);

    dataProvincias = [...dataProvinciasAux];    

    function initCharts(data, opcion) {
        filteredData = filteringData(data, opcion);
        maxData = filteringMaxData(filteredData);

        let t = select(histogram).transition().duration(2500);

        //Barra inferior
        let unaDosis = filteredData.values[filteredData.values.length - 1]['%vacunados'];
        document.getElementById('vaccine-pauta').style.width = unaDosis + '%';
        document.getElementById('poblacion-vacunada').textContent = unaDosis.replace('.',',');

        //Creación del gráfico
        margin = {top: 5, right: 5, bottom: 20, left: 50},
        width = document.getElementById('vaccine-histogram').clientWidth - margin.left - margin.right,
        height = document.getElementById('vaccine-histogram').clientHeight - margin.top - margin.bottom;

        svg = select(histogram)
            .append("svg")
            .attr('id', 'histograma')
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        //Eje X >> Mejorar formato de fecha
        x = scaleBand()
            .domain(filteredData.values.map(function(d) { return d.Publicación }))
            .range([0, width])
            .padding(0.1);

        //Mostrar eje X
        xAxis = function(svg){
            svg.call(axisBottom(x).tickFormat(function(d){
                let split = d.split("/");
                return parseInt(split[0]) + " " + meses[parseInt(split[1] - 1)]; 
            }))
            svg.call(function(g){g.selectAll('.tick').attr('opacity', function(d, i){return i == 0 || i == Math.round(filteredData.values.length / 2) || i == filteredData.values.length - 1 ? '1' : '0' })})
            svg.call(function(g){g.selectAll('.tick').attr('text-anchor', function(d, i){return i == 0 ? 'start' : i == Math.round(filteredData.values.length / 2) ? 'middle' : 'end'})})
            svg.call(function(g){g.selectAll('.tick line').remove()})
            svg.call(function(g){g.select('.domain').attr('id', 'eje-x-vacunas').attr('opacity','0')})
            svg.call(function(g){g.selectAll('.tick text').attr('class', 'axis')});
        }

        svg.append('g')
            .attr('class', 'x-axis')
            .attr('transform', 'translate(0, ' + (height) +')')
            .call(xAxis);

        //Eje Y
        y = scaleLinear()
            .domain([0,maxData])
            .range([height,0]);

        yAxis = function(svg){
            svg.call(axisLeft(y).ticks(5).tickFormat((d) => { return getNumberWithThousandsPoint(d) }))
            svg.call(function(g){g.selectAll('.tick line')            
                //.attr("stroke", "#000")
                //.attr("stroke-dasharray", function(d) {return d == 0 ? '0' : '2'})
                //.attr("stroke-opacity", function(d) {return d == 0 ? '1' : '.5'})
                .attr("stroke", function(d) {return d == 0 ? '#000' : '#dadada'})
                .attr("stroke-dasharray", function(d) {return d == 0 ? '0' : '1'})
                //.attr("x1", '0%')
                .attr("x1", '0.5%')
                .attr("x2", '' + (document.getElementById('eje-x-vacunas').getBoundingClientRect().width) + '')
            })
            svg.call(function(g){g.selectAll('.tick text').attr('class', 'axis')})
        }

        svg.append('g')
            .attr('class', 'y-axis')
            .call(yAxis);

        //Barras
        svg.selectAll('.barras')
            .data(filteredData.values)
            .enter()
            .append('rect')
            .attr('class', 'barras-pcr')
            .attr("x", function(d) { return x(d.Publicación); })
            .attr("y", function(d) { return y(d.Nuevas_diarias); })
            .attr("width", x.bandwidth())
            .attr("height", function(d) { return height - y(d.Nuevas_diarias); })
            .attr('fill', '#e0e4e9')
            .on('touchmove mouseover', function(d,i){
                //Tooltip
                let fecha = d.Publicación.split("/");
                fecha = fecha[0] + "/" + fecha[1];			
                
                let valorMedia = filteredData.values[i]['Media_7 dias'];
                
                tooltipDiv.transition()     
                    .duration(200)
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipDiv.html('<span style="font-weight: 300;">Fecha: ' + fecha + '</span><span style="font-weight: 900;">Vacunas diarias: ' + getNumberWithThousandsPoint(d.Nuevas_diarias) + '</span><span style="font-weight: 900;">Media: ' + getNumberWithThousandsPoint(valorMedia.toString().replace('.',',')) + '</span>')
                    .style("left", ""+ (event.pageX - (window.innerWidth < 939 ? 90 : 75)) +"px")     
                    .style("top", ""+ (event.pageY - 75) +"px");
            })
            .on('touchend mouseout mouseleave', function(d){
                tooltipDiv.style('display','none').style('opacity','0');
            });

        line = d3line()
            .x(function(d) { return x(d.Publicación) + x.bandwidth() / 2; })
            .y(function(d) { return y(d['Media_7 dias']); })
            .curve(curveCatmullRom.alpha(0.5));
    
        svg.append('path')
            .attr('class', 'linea-media')
            .datum(filteredData.values)
            .attr('fill', 'none')
            //.attr('stroke-width', '2.5px')
            .attr('stroke-width', '1.5px')
            .attr('stroke', '#414257')
            .attr('d', line);

        //Círculos ocultos (para mostrar tooltip)
        svg.selectAll('.circles')
            .data(filteredData.values)
            .enter()
            .append('circle')
            .attr('class', 'circulos-media')
            .attr("r", 6)
            .attr("cx", function(d) { return x(d.Publicación) + x.bandwidth() / 2; })
            .attr("cy", function(d) { return y(d['Media_7 dias']); })
            .style("fill", '#414257')
            .style('opacity', '0')
            .on('touchmove mouseover', function(d,i){
                //Tooltip
                let fecha = d.Publicación.split("/");
                fecha = fecha[0] + "/" + fecha[1];

                let valorDiario = filteredData.values[i].Nuevas_diarias;
                
                tooltipDiv.transition()     
                    .duration(200)
                    .style('display', 'block')
                    .style('opacity','0.9')
                tooltipDiv.html('<span style="font-weight: 300;">Fecha: ' + fecha + '</span><span style="font-weight: 900;">Vacunas diarias: ' + getNumberWithThousandsPoint(valorDiario) + '</span><span style="font-weight: 900;">Media: ' + getNumberWithThousandsPoint(d['Media_7 dias'].toString().replace('.',',')) + '</span>')
                    .style("left", ""+ (event.pageX - (window.innerWidth < 939 ? 90 : 60)) +"px")      
                    .style("top", ""+ (event.pageY - 75) +"px");
            })
            .on('touchend mouseout mouseleave', function(d){
                tooltipDiv.style('display','none').style('opacity','0');
            });
        
    }   

    function updateCharts(data, opcion) {
        filteredData = filteringData(data, opcion);
        maxData = filteringMaxData(filteredData);

        //Barra inferior
        let unaDosis = filteredData.values[filteredData.values.length - 1]['%vacunados'];	
        console.log(unaDosis);
        document.getElementById('vaccine-pauta').style.width = `${+unaDosis}%`;
        document.getElementById('poblacion-vacunada').textContent = unaDosis.replace('.',',');

        let t = select(histogram).transition().duration(2500);

        //Cambiamos dominio de eje Y
        y.domain([0,maxData]);
        t.select('.y-axis').call(yAxis);

        //Cambiamos datos para barras, línea y círculos
        svg.selectAll('.barras-pcr')
            .data(filteredData.values)
            .transition(t)
            .attr("x", function(d) { return x(d.Publicación); })
            .attr("y", function(d) { return y(+d.Nuevas_diarias); })
            .attr("height", function(d) { return height - y(+d.Nuevas_diarias); });

        svg.selectAll('.linea-media')
            .datum(filteredData.values)
            .transition(t)
            .attr('d', line);

        svg.selectAll('.circulos-media')
            .data(filteredData.values)
            .transition(t)
            .attr("cy", function(d) { return y(+d['Media_7 dias']); });
    }

    selector.addEventListener('change', (e) => {
        opcionDefecto = selector.value;
        updateCharts(dataProvincias, opcionDefecto);
    });

    //Primer proceso
    initCharts(dataProvincias, opcionDefecto);
}

/* Helpers para vacunación */
function filteringData(dataFilter, opcion){
    //Filtrados
    let newData = dataFilter.filter((item) => {
        if(item.key == opcion){
            return item;
        }
    })[0];

	return newData;
}

function filteringMaxData(data) {
    let auxData = max(data.values, (d) => { return +d.Nuevas_diarias });
	return auxData;
}

/* Helpers */
let normalize = (function() {
    var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç", 
        to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
        mapping = {};
   
    for(var i = 0, j = from.length; i < j; i++ )
        mapping[ from.charAt( i ) ] = to.charAt( i );
   
    return function( str ) {
        var ret = [];
        for( var i = 0, j = str.length; i < j; i++ ) {
            var c = str.charAt( i );
            if( mapping.hasOwnProperty( str.charAt( i ) ) )
                ret.push( mapping[ c ] );
            else
                ret.push( c );
        }      
        return ret.join( '' );
    }
   
})();

function comparativa(a,b){
    const datoA = normalize(a.values[0].CCAA);
    const datoB = normalize(b.values[0].CCAA);

    let comparacion = 0;
    if (datoA > datoB) {
        comparacion = 1;
    } else if (datoA < datoB) {
        comparacion = -1;
    }
    return comparacion;
}
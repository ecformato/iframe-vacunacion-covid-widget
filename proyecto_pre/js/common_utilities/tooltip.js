import { select } from 'd3-selection';
import { transition } from 'd3-transition';

let meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

export let tooltipDiv;

export function drawTooltip() {
    tooltipDiv = select("body").append("div")    
    .attr("class", "tooltip-widget")               
    .style("opacity", 0);
}

export function getOutTooltips(){
    let tooltip = document.getElementsByClassName('tooltip-widget')[0];
    tooltip.style.display = 'none';
    tooltip.style.opacity = '0';
}

export function formatTooltipDate(date){
    //Puede venir 'febrero/2007' o '7/2007' o '7/07/2020'
    let split = date.split("/");
    let dia = '', mes = '', anio = '';

    let resultado = '';

    if(isNaN(split[0])){
        mes = split[0];
        let letra = mes.charAt(0);
        letra = letra.toUpperCase();
        mes = letra + mes.slice(1);
        anio = split[1];

        resultado = mes + ' ' + anio;

    } else {
        let indice;
        if(split.length > 2){
            indice = parseInt(split[1]) - 1;
            anio = split[2];
            mes = meses[indice].toLowerCase();
            dia = split[0];

            resultado = dia + ' de ' + mes + ' ' + anio;
        } else {
            indice = parseInt(split[0]) - 1;
            anio = split[1];
            mes = meses[indice];

            resultado = mes + ' ' + anio;
        }      
        
    }
    return resultado;
}